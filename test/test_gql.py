import pytest
import json

from clipsync.queries import ClipInfoQuery, MultiVodInfoQuery, VodInfoQuery


@pytest.fixture()
def clipInfoMocks():
    with open('test/mocks/GQL_CLIP_MOCKS.json', 'r') as f:
        mocks = json.load(f)
        yield mocks


def test_getClipInfo(clipInfoMocks):
    data = ClipInfoQuery.post('NaiveEntertainingDotterelRuleFive')
    assert data == clipInfoMocks[0]['data']


def test_getVodInfoMulti():
    data = MultiVodInfoQuery.post(logins=['xqcow', 'hasanabi'])

    assert len(data['users']) == 2
    assert data['users'][0]['login'] == 'xqcow'
    assert data['users'][1]['login'] == 'hasanabi'


def test_getVodInfo():
    data = VodInfoQuery.post(login='xqcow')

    assert data['user']['login'] == 'xqcow'