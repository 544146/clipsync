import pytest
import json
import clipsync.utils as utils

from clipsync.user import User


@pytest.fixture()
def clipInfoMocks():
    with open('test/mocks/GQL_CLIP_MOCKS.json', 'r') as f:
        mocks = json.load(f)
        yield mocks


@pytest.mark.parametrize('login, id, total_seconds',
                         [('hasanabi', '817628512', 27797),
                          ('xqcow',    '817779022', 20529),
                          ('aoc',      '817942495', 11600), ])
def test_sync(login, id, total_seconds, clipInfoMocks):
    data = clipInfoMocks[0]['data']
    clipTime = utils.clipTime(clipInfo=data)
    intervalTime = User(login).sync(clipTime)

    assert intervalTime[0] == id
    assert intervalTime[1].total_seconds() == total_seconds

def test_veryStart():
    clipTime = utils.parseTime('2010-11-27T21:06:12Z')
    intervalTime = User('xqcow').sync(clipTime)

    assert intervalTime == None

def test_paginate():
    user = User('reckful')
    assert len(user.edges) == 100
    user.paginate()
    assert len(user.edges) > 100