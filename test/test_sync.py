import pytest
import json
import clipsync.utils as utils

from clipsync.queries import ClipInfoQuery
from clipsync.sync import Sync


@pytest.fixture()
def clipInfoMocks():
    with open('test/mocks/GQL_CLIP_MOCKS.json', 'r') as f:
        mocks = json.load(f)
        yield mocks



def test_syncAll(clipInfoMocks):
    LOGINS = ['forsen', 'hasanabi', 'xqcow', 'nmplol']

    data = ClipInfoQuery.post(slug='NaiveEntertainingDotterelRuleFive')
    vodInterval = utils.clipTime(clipInfo=data)
    intervalTimes = Sync(logins=LOGINS).syncAll(vodInterval=vodInterval)

    assert 'forsen' not in intervalTimes
    assert 'nmplol' not in intervalTimes

    assert intervalTimes['hasanabi'][0] == '817628512'
    assert str(intervalTimes['hasanabi'][1]) == '7:43:17'

    assert intervalTimes['xqcow'][0] == '817779022'
    assert str(intervalTimes['xqcow'][1]) == '5:42:09'