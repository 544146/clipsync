from clipsync.queries import MultiVodInfoQuery
from . import utils
from .user import User


class Sync():
    def __init__(self, logins):
        logins = list(set([l for l in logins if utils.validLogin(l)]))
        self.setupUsers(logins)

    def setupUsers(self, logins):
        self.users = []
        data = MultiVodInfoQuery.post(logins=logins)

        for state in data['users']:
            if state and 'videos' in state:
                self.users.append(
                    User(login=state['login'], edges=state['videos']['edges']))

    def syncAll(self, vodInterval):
        results = {}
        for user in self.users:
            newInterval = user.sync(vodInterval)
            if newInterval:
                results[user.login] = newInterval
        return results
