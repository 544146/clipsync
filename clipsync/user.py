from clipsync.queries import VodInfoQuery
from .edge import Edge


class User():
    def __init__(self, login, edges=None):
        self.login = login
        self.cursor = None
        self.edges = {}
        if edges:
            self.setupEdges(edges)
        else:
            self.paginate()

    def setupEdges(self, edges):
        for edge in edges:
            if edge['node']['id'] in self.edges:
                continue

            _edge = Edge(edge)
            self.edges[_edge.id] = _edge
            if edge['cursor']:
                self.cursor = edge['cursor']

    def paginate(self):
        data = VodInfoQuery.post(login=self.login, cursor=self.cursor)

        if 'user' in data and data['user']:
            self.setupEdges(data['user']['videos']['edges'])

    def sync(self, intervalTime):
        veryStart = list(self.edges.values())[-1].start

        if intervalTime < veryStart:
            if self.cursor:
                self.paginate()
                return self.sync(intervalTime)
            return None

        for edge in self.edges.values():
            if edge.start <= intervalTime <= edge.end:
                return (edge.id, intervalTime - edge.start)
